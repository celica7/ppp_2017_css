const gulp = require('gulp');
const less = require('gulp-less');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('compilarLess',() =>
   gulp.src('./less/styles.less')
   .pipe(less({
   	   outputStyle: 'nested'
   }))
   .pipe(autoprefixer({
   	   browsers: ['last 2 versions']
   }))
   .pipe(gulp.dest('./css'))
);

gulp.watch('./less/styles.less',['compilarLess']);